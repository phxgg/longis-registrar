import {Component, Input, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-instructors-dashboard-overview-exams',
  templateUrl: './instructors-dashboard-overview-exams.component.html',
  styleUrls: ['../../instructors-dashboard.component.scss']
})
export class InstructorsDashboardOverviewExamsComponent implements OnInit {
  @Input() id: any;
  public model: any;


  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }
  async ngOnInit() {
    this.model = await this._context.model('Instructors/' + this.id + '/currentExams')
      .asQueryable()
      .expand('status,examPeriod,course($expand=department)')
      .take(5)
      .getItems();


  }
}
