import { NgModule } from '@angular/core';
import { AdminModule } from '@universis/ngx-admin';

@NgModule({
    imports: [AdminModule]
})
export class AdminWrapperModule {
}
