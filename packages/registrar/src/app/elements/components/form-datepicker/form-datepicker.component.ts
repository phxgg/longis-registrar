import {Component, ElementRef, OnInit, ViewEncapsulation} from '@angular/core';
import {NgModel} from '@angular/forms';
declare var jQuery: any;

@Component({
  // tslint:disable-next-line:component-selector
  selector: '.form-date-picker',
  template: '',
  styleUrls: ['./form-datepicker.component.scss'],
  providers: [NgModel],
  encapsulation: ViewEncapsulation.None
})
export class FormDatepickerComponent implements OnInit {
  constructor(private _element: ElementRef, private ngModel: NgModel) {}

  ngOnInit() {
    jQuery(this._element.nativeElement).datepicker({
      templates: {
        leftArrow: '<i class="fas fa-chevron-left"></i>',
        rightArrow: '<i class="fas fa-chevron-right"></i>'
      },
      language: 'el'
    }).on('changeDate', (ev) => {
      const value = jQuery(this._element.nativeElement).datepicker('getDate');
      this.ngModel.viewToModelUpdate(value);
      this.ngModel.control.markAsDirty();
    });
    this.ngModel.valueChanges.subscribe((value) => {
      jQuery(this._element.nativeElement).datepicker('setDate', value);
    });
  }
}
