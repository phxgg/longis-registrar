import { Component, OnInit, ViewChild, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {
  AdvancedRowActionComponent,
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import { AngularDataContext } from '@themost/angular';
import {Observable, Subscription} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {AppEventService, DIALOG_BUTTONS, ErrorService, LoadingService, ModalService, UserActivityService} from '@universis/common';
import {EditCoursesComponent} from '../edit-courses/edit-courses.component';
import {AddCoursesComponent} from '../add-courses/add-courses.component';
import {AdvancedSearchFormComponent} from '@universis/ngx-tables';
import { TableConfigurationResolver } from '../../../../registrar-shared/table-configuration.resolvers';
import { CreateCourseComponent } from '../create-course/create-course.component';

@Component({
  selector: 'app-study-programs-preview-courses',
  templateUrl: './study-programs-preview-courses.component.html'
})
export class StudyProgramsPreviewCoursesComponent implements OnInit, OnDestroy {

  private dataSubscription: Subscription;
  private paramSubscription: Subscription;
  private fragmentSubscription: Subscription;
  private studyProgram: any;
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('courses') courses: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();
  public recordsTotal: any;
  private selectedItems = [];
  private specialization: any;
  private copyRulesData = null;
  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _translateService: TranslateService,
              private _userActivityService: UserActivityService,
              private _router: Router,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _loadingService: LoadingService,
              private _appEvent: AppEventService,
              private resolver: TableConfigurationResolver)  { }

  async ngOnInit() {

    this.dataSubscription = this._activatedRoute.data.subscribe(data => {
      // get query params
      if (this.paramSubscription) {
        this.paramSubscription.unsubscribe();
      }
      this.fragmentSubscription = this._activatedRoute.fragment.subscribe((fragment) => {
        if (fragment === 'reload') {
          this.courses.fetch();
        }
      });
      this.paramSubscription = this._activatedRoute.params.subscribe((params) => {
        if (data.tableConfiguration) {
          this.studyProgram = parseInt(params.id, 10) || 0;
          const queryParams = this._context.model('SpecializationCourses')
              .where('studyProgramCourse/studyProgram').equal(this.studyProgram)
              .and('specializationIndex').equal(params.specialization)
              .getParams();
          data.tableConfiguration.defaults.filter = queryParams.$filter;
          this.courses.config = AdvancedTableConfiguration.cast(data.tableConfiguration, true);
          this.courses.fetch(true);
        }
        if (data.searchConfiguration) {
          this.search.form = data.searchConfiguration;
          this.search.formComponent.formLoad.subscribe((res: any) => {
            Object.assign(res, { studyProgram: this._activatedRoute.snapshot.params.id, activeDepartment: data.department.id });
          });
        }
        // add user activity
        this._context.model('StudyProgramSpecialties')
            .where('studyProgram').equal(params.id)
            .and('specialty').equal(params.specialization)
            .select('id', 'specialty', 'studyProgram/id as studyProgram' , 'studyProgram/name as studyProgramName', 'name')
            .getItem().then((item) => {
              if (item) {
                this._userActivityService.setItem({
                  category: this._translateService.instant('StudyPrograms.TitlePlural'),
                  description: `${item.studyProgramName} ${item.name}`,
                  url: window.location.hash.substring(1), // get the path after the hash
                  dateCreated: new Date
                });
                this.specialization = item;
              }
        });

      });

    });

  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }

  async createClassesAction() {
    try {
      this._loadingService.showLoading();
      // const items = await this.getSelectedItems();
      let items = this.courses.selected.map((x) => {
        return {
          id: x.id,
          courseStructureType: x.courseStructureType,
          course: x.courseId,
          specialtyId: x.specialtyId
        };
      });
      // filter out complex courses
      this.selectedItems = items.filter(course => course.courseStructureType !== 4);
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Courses.CreateClassesAction.Title',
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'CourseClasses/create' : null,
          description: 'Courses.CreateClassesAction.Description',
          errorMessage: 'Courses.CreateClassesAction.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeCreateClassesAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeCreateClassesAction() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (data.year == null || data.year === '' || data.period == null || data.period === '') {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            // and create class
            await this._context.model(`Courses/${encodeURIComponent(item.course)}/createClass`).save(data);
            try {
              await this.courses.fetchOne({
                id: item.course
              });
            } catch (err) {
              //
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  executeEditAction() {
    return new Observable((observer) => {
      this._context.model('SpecializationCourses').save(this.selectedItems).then(() => {
        this.courses.fetch(true);
        observer.next();
      }).catch((err) => {
        observer.error(err);
      });
    });
  }


  editMany() {
        if (this.courses.selected.length === 1) {
          // open edit modal window
          const urlTree = this._router.createUrlTree([{
            outlets: {
              modal: [ 'item', this.courses.selected[0].id, 'edit' ]
            }
          }], {
            relativeTo: this._activatedRoute
          });
          return this._router.navigateByUrl(urlTree).catch((err) => {
            this._errorService.showError(err, {
              continueLink: '.'
            });
          });
        } else {
          this.selectedItems = this.courses.selected.map((x) => {
            return {
              id: x.id,
              units: x.units,
              coefficient: x.coefficient,
              semester: x.semester,
              ects: x.ects,
              courseType: x.courseType
            };
          });
          this._modalService.openModalComponent(EditCoursesComponent, {
            class: 'modal-lg',
            keyboard: false,
            ignoreBackdropClick: true,
            initialState: {
              items: this.selectedItems,
              modalTitle: 'StudyPrograms.EditCourses',
              execute: this.executeEditAction()
            }
          });
        }
    }

  async removeMany() {
    const items = this.courses.selected.map((x) => {
      return {
        id: x.id,
        courseStructureType: x.courseStructureType,
        course: x.courseId,
        specialtyId: x.specialtyId
      };
    });
    if (items.length > 0) {
      // check if items contains complex or course parts courses
      const complexSelected = items.filter(x => {
        return x.courseStructureType === 4;
      });
      const coursePartsSelected = items.filter(x => {
        return x.courseStructureType === 8;
      });
      let itemsAdded = false;
      if (coursePartsSelected.length > 0) {
        for (let i = 0; i < coursePartsSelected.length; i++) {
          // get parent course and add it to selectedItems
          const coursePartCourse = await this._context.model('Courses')
            .where('id').equal(coursePartsSelected[i].course).getItem();
          // check if complexCourse exist at complexSelected array
          const found = complexSelected.find(x => {
            return x.course === coursePartCourse.parentCourse;
          });
          if (!found) {
            // get Specialization course for parentCourse
            const specializationCourse = await this._context.model('SpecializationCourses')
              .where('studyProgramCourse/course').equal(coursePartCourse.parentCourse)
              .and('specialization/id').equal(coursePartsSelected[i].specialtyId)
              .getItem();
            if (specializationCourse) {
              const complexCourse = {
                id: specializationCourse.id,
                courseStructureType: 4,
                course: coursePartCourse.parentCourse,
                specialtyId: specializationCourse.specialization
              };
              // add parentCourse to arrays
              items.push(complexCourse);
              complexSelected.push(complexCourse);
              itemsAdded = true;
            }
          }
        }
      }

      if (complexSelected.length > 0 ) {
        for (let i = 0; i < complexSelected.length; i++) {
          const complexCourse = await this._context.model('Courses')
            .where('id').equal(complexSelected[i].course).expand('courseParts').getItem();
          if (complexCourse && complexCourse.courseParts && complexCourse.courseParts.length > 0) {
            // check if courseParts exist at coursePartsSelected array
            for (let j = 0; j < complexCourse.courseParts.length; j++) {
              const coursePart = complexCourse.courseParts[j];
              const found = coursePartsSelected.find(x => {
                return x.course === coursePart.id;
              });
              if (!found) {
                // get Specialization course
                const specializationCourse = await this._context.model('SpecializationCourses')
                  .where('studyProgramCourse/course').equal(coursePart.id)
                  .and('specialization/id').equal(complexSelected[i].specialtyId).getItem();
               if (specializationCourse) {
                 items.push(specializationCourse);
                 itemsAdded = true;
               }
              }
            }
          }
        }
      }
      const title = this._translateService.instant('StudyPrograms.RemoveCourses');
      // tslint:disable-next-line:max-line-length
      const  message = itemsAdded ? this._translateService.instant('StudyPrograms.RemoveCoursesAdditionalMessage') : this._translateService.instant('StudyPrograms.RemoveCoursesMessage');
      this._modalService.showDialog(title, message, DIALOG_BUTTONS.YesNo).then( dialogResult => {
        if (dialogResult === 'no') {
          return;
        }
        this._loadingService.showLoading();
        this._context.model('SpecializationCourses').remove(items).then(() => {
          this.courses.fetch(true);
          this._loadingService.hideLoading();
        }).catch((err) => {
          this._loadingService.hideLoading();
          this._errorService.showError(err);
        });
      });
    }
  }

  async copy() {
    // Only one row can be copied
    if (this.courses.selected.length === 1) {

      const courseID = this.courses.selected[0].courseId;

      const programCourseObject = await this._context.model('ProgramCourses')
      .where('program').equal(this.studyProgram)
      .and('course').equal(courseID)
      .getItem();

        if (programCourseObject) { // If programCourse ID exists then get the rules
          this._loadingService.showLoading();
          const courseRules = await this._context.model(`ProgramCourses/${programCourseObject.id}/RegistrationRules`).getItems();
          if (courseRules.length === 0) {
            const title = this._translateService.instant('StudyPrograms.CopyRules');
            const message = this._translateService.instant('StudyPrograms.CopyRulesAdditionalMessage');
            this._modalService.showDialog(title, message, DIALOG_BUTTONS.Ok).then(dialogResult => {
              return;
            });
          } else {
            this.copyRulesData = {
              source: programCourseObject.id,
              additionalType: courseRules[0].additionalType,
              targetType: courseRules[0].targetType
            };
          }
          this._loadingService.hideLoading();
        } else { // programCourseObject is null - show error modal
          const title = this._translateService.instant('StudyPrograms.CopyRulesError');
          const message = this._translateService.instant('StudyPrograms.CopyRulesErrorMessage');
          this._modalService.showDialog(title, message, DIALOG_BUTTONS.Ok).then(dialogResult => {
            return;
          });
        }
      this.courses.selectNone();
    }
  }

  async paste() {
    // Show confirmation modal first
    try {
      const title = this._translateService.instant('StudyPrograms.PasteRules');
      const message = this._translateService.instant('StudyPrograms.PasteRulesAdditionalMessage');
      this._modalService.showDialog(title, message, DIALOG_BUTTONS.YesNo).then(async dialogResult => {
        if (dialogResult === 'no') {
          this.courses.selectNone();
          return;
        }
        this._loadingService.showLoading();

        await Promise.all(this.courses.selected.map(async course => {
          const courseID = course.courseId;

          // Get programCourse ID for each of the selected courses
          return this._context.model('ProgramCourses')
            .where('program').equal(this.studyProgram)
            .and('course').equal(courseID)
            .getItem().then(async (programCourseObject) => {
              if (programCourseObject) {
                if (this.copyRulesData.source !== programCourseObject.id) {
                  this.copyRulesData.destination = programCourseObject.id;
                  await this._context.model(`Rules/copyRules`).save(this.copyRulesData);
                }
              } else { // programCourseObject is null - show error modal
                this._modalService.showDialog(this._translateService.instant('StudyPrograms.PasteRulesError'),
                  this._translateService.instant('StudyPrograms.PasteRulesErrorMessage'), DIALOG_BUTTONS.Ok).then(result => {
                  return;
                });
              }
            });
        }));
        this.courses.selectNone();
        this._loadingService.hideLoading();
      });
    } catch (err) {
      this.courses.selectNone();
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  add() {
    try {
      // this.resolver.get('Courses', 'selectSimpleComplex').subscribe((tableConfig) => {});
      this._modalService.openModalComponent(CreateCourseComponent, {
        class: 'modal-xl modal-table',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          data: {
            department: this._activatedRoute.snapshot.data.department.id,
          },
          target: this.specialization,
          modalTitle: 'StudyPrograms.AddCourses',
          execute: (() => {
            return new Observable((observer) => {
              this._loadingService.showLoading();
              // get add courses component
              const component = <CreateCourseComponent>this._modalService.modalRef.content;
              const course = component.items[0];
              // remove null or empty properties
              Object.keys(course).forEach((key) => {
                if (course[key] == null
                  || course[key] === ''
                  || (typeof course[key] === 'object' && Object.keys(course[key]).length === 0)) {
                  delete course[key];
                }
              });
              const studyProgram = this.studyProgram;
              const specializationCourse = {
                ects: course.ects != null ? course.ects : 1,
                coefficient: 1,
                units: 1,
                semester: 1,
                courseType: null,
                studyProgramCourse: {
                  course,
                  studyProgram
                },
                specialization: this.specialization,
                '$state': 1
              }

              // and submit
              this._context.model('SpecializationCourses').save(specializationCourse).then(() => {
                this.courses.fetch(true);
                this._loadingService.hideLoading();
                observer.next();
              }).catch((err) => {
                this._loadingService.hideLoading();
                observer.error(err);
              });
            });
          })()
        }
      });
    } catch (err) {
      if (this._modalService.modalRef) {
        this._modalService.modalRef.hide();
      }
          this._errorService.showError(err, {
            continueLink: '.'
          });
     }
  }

}
