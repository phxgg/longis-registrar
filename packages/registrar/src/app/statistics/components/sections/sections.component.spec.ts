import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SectionsComponent } from './sections.component';
import { TranslateModule } from '@ngx-translate/core';
import { APP_BASE_HREF, CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { StatisticsRoutingModule } from './../../statistics.routing';
import { NgArrayPipesModule } from 'ngx-pipes';
import { ReportsSharedModule } from '../../../reports-shared/reports-shared.module';
import { StatisticsService } from './../../services/statistics-service/statistics.service';
import { HomeComponent } from './../../components/home/home.component';
import { SettingsModule } from './../../../settings/settings.module';
import { MostModule } from '@themost/angular';
import { SettingsService } from './../../../settings-shared/services/settings.service';
import { AuthModule, ConfigurationService, ErrorService, LoadingService, SharedModule } from '@universis/common';
import { TestingConfigurationService } from '@universis/common/testing';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { BsModalService, ModalModule } from 'ngx-bootstrap';
import { RouterTestingModule } from '@angular/router/testing';
import { routes } from './../../statistics.routing';
import { Observable } from 'rxjs';
import { By } from '@angular/platform-browser';

describe('SectionsComponent', () => {
  let component: SectionsComponent;
  let fixture: ComponentFixture<SectionsComponent>;
  let mockStatisticsService;

  beforeEach(() => {
    mockStatisticsService = jasmine.createSpyObj(['getReportTemplatesOfCategory', 'getStatisticsReportCategories']);
    mockStatisticsService.getStatisticsReportCategories.and.returnValue([]);
    mockStatisticsService.getReportTemplatesOfCategory.and.returnValue([]);

    return TestBed.configureTestingModule({
      imports: [
        CommonModule,
        AuthModule,
        TranslateModule.forRoot(),
        StatisticsRoutingModule,
        FormsModule,
        SharedModule.forRoot(),
        ModalModule.forRoot(),
        SettingsModule,
        RouterTestingModule.withRoutes(routes),
        MostModule.forRoot({
          base: '/',
          options: {
            useMediaTypeExtensions: false
          }
        }),
        NgArrayPipesModule,
        ReportsSharedModule.forRoot()
      ],
      declarations: [HomeComponent, SectionsComponent],
      providers: [
        SettingsService,
        StatisticsService,
        LoadingService,
        ErrorService,
        BsModalService,
        {
          provide: ConfigurationService,
          useClass: TestingConfigurationService
        },
        {
          provide: APP_BASE_HREF,
          useValue: '/'
        },
        {
          provide: ActivatedRoute,
          useValue: {
            params: Observable.of({
              category: 'mockCategory'
            })
          }
        }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should read the current category from the router data', () => {
    fixture.detectChanges();
    expect(component.category).toBe('mockCategory');
  });

  it('should list all the given list items', () => {
    component.reportTemplates = [
      {
        id: 1,
        name: 'mockName',
        description: 'mock description'
      },
      {
        id: 2,
        name: 'mockName2',
        description: 'another mock description '
      },
      {
        id: 3,
        name: 'mockName3',
        description: 'another mock description '
      }
    ];
    const originalList = fixture.debugElement.queryAll(By.css('.card'));
    fixture.detectChanges();
    const itemList = fixture.debugElement.queryAll(By.css('.card'));
    expect(originalList.length).toBe(0);
    expect(itemList.length).toBe(3);
  });

  it('should search by name', () => {
    component.reportTemplates = [
      {
        id: 1,
        name: 'mockName',
        description: 'mock description'
      },
      {
        id: 2,
        name: 'mockName2',
        description: 'another mock description '
      },
      {
        id: 3,
        name: 'mockName3',
        description: 'another mock description '
      }
    ];
    fixture.detectChanges();
    const originalList = fixture.debugElement.queryAll(By.css('.card'));
    component.search = 'mockName3';
    fixture.detectChanges();
    const list = fixture.debugElement.queryAll(By.css('.card'));
    expect(originalList.length).toBe(3);
    expect(list.length).toBe(1);
  });

  it('should search by description', () => {
    component.reportTemplates = [
      {
        id: 1,
        name: 'mockName',
        description: 'mock description'
      },
      {
        id: 2,
        name: 'mockName2',
        description: 'another mock description'
      },
      {
        id: 3,
        name: 'mockName3',
        description: 'another mock description'
      }
    ];
    fixture.detectChanges();
    const originalList = fixture.debugElement.queryAll(By.css('.card'));
    component.search = 'another mock description';
    fixture.detectChanges();
    const list = fixture.debugElement.queryAll(By.css('.card'));
    expect(originalList.length).toBe(3);
    expect(list.length).toBe(2);
  });
});
