import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartmentsPreviewComponent } from './departments-preview.component';

describe('DepartmentsPreviewComponent', () => {
  let component: DepartmentsPreviewComponent;
  let fixture: ComponentFixture<DepartmentsPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartmentsPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentsPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
