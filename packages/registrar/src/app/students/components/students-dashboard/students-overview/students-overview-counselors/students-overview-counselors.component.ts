import {Component, Input, OnChanges, OnDestroy, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-students-overview-counselors',
  templateUrl: './students-overview-counselors.component.html'
})

export class StudentsOverviewCounselorsComponent implements OnInit, OnDestroy  {

  public counselors: any;
  @Input() studentId: number;
  private subscription: Subscription;
  private fragmentSubscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.studentId = params.id;
      this.counselors = await this._context.model('StudentCounselors')
        .where('student').equal(this.studentId)
        .expand('instructor')
        .getItems();
    });
    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(async (fragment) => {
      if (fragment && fragment === 'reload') {
        this.counselors = await this._context.model('StudentCounselors')
          .where('student').equal(this.studentId)
          .expand('instructor')
          .getItems();
      }
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }
}
