import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MostModule} from '@themost/angular';
import {HttpHandler} from '@angular/common/http';

@Component({
  selector: 'app-advanced-search',
  templateUrl: './advanced-search.component.html',
  styles: []
})
export class AdvancedSearchComponent implements OnInit {
  filter = {
    searchStatus: '',
    searchSubject: '',
    searchStudentName: '',
    searchAcademicYear: '',
    searchAcademicPeriod: '',
    searchStartDate: ''
  };
  intershipStatusTypes = {
    value : {
    }
  };
  intershipAcademicYears = {
    value: {

    }
  };
  intershipAcademicPeriods = {
    value: {

    }
  }

  constructor() { }

  ngOnInit() {
  }

}
